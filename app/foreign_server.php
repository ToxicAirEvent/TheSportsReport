<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class foreign_server extends Model
{
    use HasFactory;

    public $incrementing = false;
}
