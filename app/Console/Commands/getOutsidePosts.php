<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

use App\highlight;
use App\foreign_server;
use App\foreign_user;
use App\job;

class getOutsidePosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:get-outside-posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawls all json feeds of posts from foreign servers you want imported to your server.';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        $job_status = job::find('get_foreign_posts');

        if($job_status->job_status === true){
            echo "Post crawler is already importing posts. Exiting until next time.";
            return false;
        }

        $job_status->job_status = true;
        $job_status->save();


        $servers = foreign_server::get();

        foreach($servers as $server){

            $highlights_from_server = highlight::where('foreign_server_id', '=', $server->id)->get();

            try{
                $highlight_req = Http::get($server->endpoint);
                $highlight_feed = $highlight_req->json(); 
            }catch(Exception $e){
                Log::error("The request to another servers highlight feed was malformed, unavailable, or the server was down.");
                Log::error($e);
                continue;
            }

            $foreign_posters = Arr::pluck($highlight_feed,'poster_info');

            $this->updateOrInsertHighlights($server->id, $highlight_feed, $highlights_from_server);
            $this->addOrUpdateForeignPosters($server->id, $foreign_posters);
            $this->cleanUpOldPosts($server->id, $server->store_count);
        }

        //Once post insertion completes set the job status to false.
        $job_status->job_status = false;
        $job_status->save();
    }

    private function updateOrInsertHighlights($server_id, $highlight_feed, $existing_highlights){
        foreach($highlight_feed as $highlight){
                //Bind the user ID (or null user) to the data to be inserted. As well as some other info about the highlights origins.
                $highlight['foreign_server_id'] = $server_id;

                if(!isset($highlight['poster_info']['id'])){
                    $highlight['poster_id'] = null;
                }else{
                    $highlight['poster_id'] = $highlight['poster_info']['id'];
                }
                
                unset($highlight['poster_info']);
                unset($highlight['human_time']);
                unset($highlight['share_link']);

                //Parse and bind updated timestamps for the arrays.
                $highlight['created_at'] = Carbon::parse($highlight['created_at'])->toDateTimeString();
                $highlight['updated_at'] = Carbon::parse($highlight['updated_at'])->toDateTimeString();

                if($existing_highlights->contains('foreign_post', $highlight['foreign_post'])){
                    highlight::where('foreign_server_id', '=', $highlight['foreign_server_id'])
                        ->where('foreign_post', '=', $highlight['foreign_post'])
                        ->update($highlight);
                }else{
                    highlight::insert($highlight);
                }
            }
    }

    private function addOrUpdateForeignPosters($server_id, $posters){
        $poster_ids = [];

        foreach($posters as $poster){
            if(is_array($poster) && !in_array($poster['id'], $poster_ids)){
                $poster['foreign_server_id'] = $server_id;

                foreign_user::updateOrCreate(['foreign_server_id' => $server_id, 'id' => $poster['id']], $poster);
                array_push($poster_ids, $poster['id']);
            }
        }
    }

    private function cleanUpOldPosts($server_id, $post_stored){
        $post_in_store_limit = highlight::where('foreign_server_id', '=', $server_id)->orderby('created_at', 'desc')->take($post_stored)->get();

        $oldest_in_limit = $post_in_store_limit->last();

        highlight::where('foreign_server_id', '=', $server_id)->where('created_at', "<=", $oldest_in_limit->created_at)->delete();
    }
}
