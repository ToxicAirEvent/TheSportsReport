<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use App\highlight;
use App\job;

class optimizeVideo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:optimize-video';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs the FFMPEG video optimizer in the background of the server.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $uncompressed_vids = highlight::where('service_origin', '=', 'organic_video')->where('optimized_file','=',null)->get();

        $update_status = job::find('optimize_videos');

        if($update_status->job_status === true){
            echo "Video optimization job is already running. Will try again later.";
            return false;
        }

        if($uncompressed_vids->count() <= 0){
            echo "No videos in the queue to optimize.";
            return false;
        }

        /*
            Videos in need of optimization are checked for every few minutes but we don't want to bog down the server.
            So we lock out the status of this job from running until previous executions are entirely completed.
        */
        $update_status->job_status = true;
        $update_status->save();

        foreach($uncompressed_vids as $vid){

            //Create a file name for the optimized video.
            $new_file_name = "opt_" . time() . "_" . Str::random(16);

            //Get the path for the existing file (f_path) and set the file path for the optimized file (o_path)
            $o_path = Storage::disk('video_uploads')->path('converted/');
            //str_replace Temp fix to strip '/videos' from front of upload http relative path for display of the video
            $f_path = Storage::disk('video_uploads')->path(str_replace("/videos/","",$vid->origin_url));
            $l_path = Storage::disk('local')->path('logs/');

            /*
                A series of variables that will combine to make an ffmpeg query.
                It could all be one line but it can get a bit hard to parse so it is easier to build the string as part, and then put the parts together.

                By default we're compressing to HD video if the frame is above that, setting a constant framerate of 30fps and a bit rate of 10MB/s.
                This should give a smooth frame rate at a high bit rate and resolution while keeping the file decently small.
            */
            $quality_settings = "-vf \"scale='min(1920,iw)':'min(1080,ih)', fps=30\" -b:v 10M";
            $file_locations = "-i " . $f_path . " " . $o_path . $new_file_name . ".mp4";
            $logging_path = "2> " . $l_path . "ffmpeg.log";

            $ffmpeg_command = "ffmpeg -y " . $file_locations . " " . $quality_settings . " " . $logging_path;

            $this_vid = highlight::find($vid->highlight_id);

            shell_exec($ffmpeg_command);

            $this_vid->optimized_file = "/videos/converted/" . $new_file_name . ".mp4";
            $this_vid->save();
        }

        $update_status->job_status = false;
        $update_status->save();
        echo "Video conversion complete.";
    }
}
