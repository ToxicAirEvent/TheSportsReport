<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class AdminFirstUser extends Command
{
    protected $signature = 'admin:first-user';

    protected $description = 'Make the first ever created User an admin';

    public function handle()
    {
        $user = User::orderBy('created_at')->first();

        if ($user) {
            $user->admin = 1;
            $user->poster = 1;
            $user->save();

            $this->info('User "' . $user->name . '" is now an admin.');
        } else {
            $this->info('You don\'t have any users yet. Go to your website and navigate to the /register route. Make an account. Then run this command.');
        }
    }
}