<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\rss_feed;
use App\highlight;
use App\job;

use Feeds;
use Carbon\carbon;

class rssFeedCrawler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:rss-feed-crawler';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs a crawl and imports all posts from RSS feeds registered in the sites database.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $all_feeds = rss_feed::all();
        $update_status = job::find('get_rss_feed_posts');
        $server_tz = config('app.timezone');

        if($update_status->job_status === true){
            echo "RSS feeds are already being crawled. Job won't execute twice/concurrently.";
            return false;
        }

        if($all_feeds->count() <= 0){
            echo "There are no RSS feeds to crawl.";
            return false;
        }

        $update_status->job_status = true;
        $update_status->save();

        $recent_rss = highlight::where('service_origin','=','rss')->where('created_at','>=',Carbon::now()->subDays(30))->pluck('permalink')->all();

        foreach($all_feeds as $feed){
            $fetched_feed = Feeds::make($feed->feed_url);

            $feed_data = [
                'title' => $fetched_feed->get_title(),
                'permalink' => $fetched_feed->get_permalink(),
                'items' => $fetched_feed->get_items(),
            ];

            $feed_crawled = rss_feed::find($feed->id);
            $feed_crawled->last_crawled = Carbon::now()->format('Y-m-d H:i:s');
            $feed_crawled->save();

            foreach($feed_data['items'] as $post){

                $post_pl = trim($post->get_permalink());
                $post_time = trim($post->get_date());
                $post_title = trim($this::cleanUpText($post->get_title()));
                $post_summary = trim($this::cleanUpText($post->get_description(), 280));

                if($post_summary != null){
                    $post_summary = (strlen($post_summary) > 140) ? substr($post_summary,0,140)."..." : $post_summary;
                }

                $post_thumbnail = $this::imageEnclosureCheck($post->get_enclosure());

                //If the post isn't already in the system, it has a valid timestamp, and title, we'll log it to the database.
                if(!in_array($post_pl, $recent_rss) && isset($post_time) && $post_title != null){

                    //Conform the post time to what the Framework expects timestamps to be in and the timezone of the app. Set in /config/app.php
                    $post_time = $formattedTime = Carbon::parse($post_time)->setTimezone($server_tz)->format('Y-m-d H:i:s');

                    highlight::insert([
                        'highlight_title' => $post_title,
                        'origin_url' => $post_pl,
                        'highlight_description' => $post_summary,
                        'permalink' => $post_pl,
                        'url_slug' => $this::rssUrlSlugger(trim($post->get_title())),
                        'embed_data' => $post_thumbnail,
                        'service_origin' => 'rss',
                        'poster_id' => $feed->id,
                        'created_at' => $post_time,
                        'updated_at' => $post_time,
                    ]);
                }
            }
        }

        //Once the feed crawl is done set the job status back to false.
        $update_status->job_status = false;
        $update_status->save();
    }

    private static function rssUrlSlugger($post_title){
        // Convert spaces to dashes
        $post_title = str_replace(' ', '-', $post_title);

        // Remove non-alphanumeric characters
        $post_title = preg_replace('/[^a-zA-Z0-9\-]/', '', $post_title);

        $post_title = (strlen($post_title) > 100) ? substr($post_title,0,100) : $post_title;

        $post_title = $post_title . "_" . Carbon::now()->timestamp;

        return strtolower($post_title);
    }

    private static function cleanUpText($string, $cut_length = 140) {
        // Strip HTML tags - Remove tags first so attributes encapsulated in them aren't left and mistakenly left by the character replacer.
        $string = strip_tags($string);

        // Remove characters that aren't single quotes, double quotes, dashes, letters, numbers, spaces, parenthesis, @ symbol, # symbol, or currency characters
        $string = preg_replace("/[^\p{L}\p{N}\s'\"@\#\-\(\)\p{Sc}]/u", '', $string);

        // Cut off string if it exceeds 280 characters and append '...'
        if (strlen($string) > $cut_length) {
            $string = substr($string, 0, $cut_length) . '...';
        }

        return $string;
    }

    private static function imageEnclosureCheck($enclosure_obj){

        if($enclosure_obj->link === null){
            return null;
        }

        $valid_image_types = ["gif","png","webp","jpg","jpeg"];

        $extension_index = strrpos($enclosure_obj->link,".");
        $file_extension = strtolower(substr($enclosure_obj->link, $extension_index + 1));

        if(in_array($file_extension, $valid_image_types)){
            return '<img src="' . $enclosure_obj->link . '" class="img-fluid">';
        }

        return null;
    }
}