<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;

class approvedPosterCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        if(env('APPROVED_POSTERS_ONLY', 1) === 0 && Auth::check()){
            return $next($request);
        }

        if(Auth::check() && (Auth::user()->admin === 1 || Auth::user()->poster === 1)){
            return $next($request);
        }
        
        return response(view('middlewareScreens.posting_blocked'));
    }
}
