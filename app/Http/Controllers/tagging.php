<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\tag_model;
use App\highlight;

class tagging extends Controller
{
	public function tagTranslator(Request $tag_data){

		//Validate the tags content for insertion into the database.
		$tag_data_validation = Validator::make($tag_data->all(), [
			'highlight_id' =>'required|exists:highlights,highlight_id',
			'tags' => ['required', 'regex:^[a-zA-Z0-9,.!? ]*$^','filled','max:100']
		]);

		if ($tag_data_validation->fails()) {
			return back()->withInput()->withErrors($tag_data_validation);
		}

		$highlight_data = highlight::where('highlight_id','=', $tag_data->highlight_id)->firstOrFail();

		/*
			Only admins should be allowed to tag foreign posts. So if the foreign_post id is set we'll need to make sure the user is an admin.
			If the post is a local post then we'll need to make sure the user is the one who made the post, or is an admin.

			If they are we'll let the tag addition be processed.

			Uses non-strict comparison operation because poster_id is stored as a string, at least returns as one from the database.
			This should potentially be fixed, but also might consider making user ids UUID4 which would become a string to string comparison.
		*/
		if(isset($highlight_data->foreign_post) && Auth::user()->admin < 1){
			return back()->withErrors(['not_authorized' => 'Only admins can manage foreign posts.']);
		}

		if(Auth::user()->id == $highlight_data->poster_id || Auth::user()->admin >= 1){
			$this->insertTags(explode(",", $tag_data->tags), $tag_data->highlight_id);
			return back();
		}

		return back()->withErrors(['not_authorized' => 'You cannot add tags to a post unless you created it, or are an admin.']);
	}

	private function insertTags($tag_bag, $highlight_id){
		//Empty collection tag data will be fed into.
		$full_tag_set = collect();

		$number_of_tags = count($tag_bag);

		for($i = 0;$i < $number_of_tags; $i++){

			$create_url_tag = preg_replace('/[^\w]/', '_', strtolower($tag_bag[$i]));

			$full_tag_set->push(['friendly_tag' => $tag_bag[$i],'url_tag' => $create_url_tag]);
		}

		foreach($full_tag_set as $single_tag){
			tag_model::insertNewTags($highlight_id, $single_tag['friendly_tag'], $single_tag['url_tag']);
		}
	}
}
