<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\carbon;

use App\highlight;
use App\tag_model;
use App\User;
use App\foreign_user;
use App\rss_feed;



class highlightFeed extends Controller
{
	public function homePageFeed(){
		$highlight_feed = highlight::orderBy('created_at','desc')->simplePaginate(env('POST_PER_PAGE', 10));

		$highlight_feed = $this->addHighlightsMeta($highlight_feed);

		return view('homePageFeed',['feed_data' => $highlight_feed]);
	}

	public function singlePost($post_id,$friendly_slug){
		$single_post_data = highlight::where('highlight_id','=',$post_id)->firstOrFail();

		/*
		 * If the slug in the URL doesn't match the one in the database redirect to the correct URL.
		 * This stops duplicate data URLs which hurt our SEO.
		 */
		if($friendly_slug != $single_post_data->url_slug){
			$correct_slug = 'highlight/' . $post_id . '/' . $single_post_data->url_slug;

			return redirect($correct_slug);
		}

		$post_tags = tag_model::getPostTags($post_id);

		$single_post_data = $this->getPosterData($single_post_data);

		$single_post_data->setAttribute('post_tags', $post_tags);
		$single_post_data->setAttribute('human_time', $this::easilyReadableDate($single_post_data->created_at));

		//Determine if the user is logged in, is an admin, and is OP. Flag that on the post for showing contextual actions.
		$single_post_data->setAttribute('is_authed', Auth::check());
		$single_post_data->setAttribute('authed_id', $single_post_data->is_authed ? Auth::user()->id : false);
		$single_post_data->setAttribute('is_admin', $single_post_data->is_authed ? Auth::user()->admin : false);

		return view('singleHighlight',['highlight_data' => $single_post_data]);
	}

	/*
	 *We want all our URL's to have words so people can tell what the page contains and so can Google.
	 *If anyone accesses a page just using the post ID this will redirect them to the page with the URL slug as part of the URL.
	*/
	public function sluglessSinglePost($post_id){
		$highlight_data = highlight::where('highlight_id','=',$post_id)->firstOrFail();

		$post_with_slug_url = 'highlight/' . $post_id . '/' . $highlight_data->url_slug;

		return redirect($post_with_slug_url);
	}

	public function highlightsByTag($tag_url){
		$post_ids_in_tag = tag_model::getPostByTag($tag_url);

		$post_under_tag = highlight::postInTag($post_ids_in_tag);

		return view('tagFeed',['feed_data' => $post_under_tag,"meta_tag" => $tag_url]);
	}

	public function filterHighlights($mode = 'out', $type_list){

		$types_array = explode(",",$type_list);

		if($mode === 'out'){
			$filtered_highlights = highlight::whereNotIn('service_origin',$types_array)->orderBy('created_at','desc')->simplePaginate(env('POST_PER_PAGE', 10));
		}else{
			$filtered_highlights = highlight::whereIn('service_origin',$types_array)->orderBy('created_at','desc')->simplePaginate(env('POST_PER_PAGE', 10));
		}

		$filtered_highlights = $this->addHighlightsMeta($filtered_highlights);

		return view('homePageFeed',['feed_data' => $filtered_highlights]);
	}

	public function apiFeed($last = 20){
		//Cap last set of post retrieved to be 100 or less.
		if($last > 100){
			$last = 100;
		}

		//Only posts organically made to your server should be broadcast out. This means foreign posts and RSS feeds you import won't be sent to other servers.
		$highlight_feed = highlight::whereNull('foreign_post')->where('service_origin', '!=', 'rss')->orderBy('created_at','desc')->take($last)->get();
		$highlight_feed->makeHidden(['foreign_server_id','highlight_id']);

		$highlight_feed = $this->addHighlightsMeta($highlight_feed, false);

		/*
			BUILD DIRECT LINKS

			Since this feed data is going to be placed on other websites we need all of the links to go directly to this website.
			As opposed to populating relative paths which could change later or not work entirely on someone elses platform.
		*/
		foreach($highlight_feed as $item){

			//Update video URLs
			if($item->service_origin === 'organic_video'){
				$item->origin_url = env('APP_URL') . $item->origin_url;

				if(isset($item->optimized_file)){
					$item->optimized_file = env('APP_URL') . $item->optimized_file;
				}
			}

			/*
				UPDATE KEYS FOR BROADCAST

				To make it easier to directly update posts on sites importing the API feed some keys need to be rebound to associate certain first party IDs
				to match the format of what they'll be entered in as foreign IDs on the other server.
			*/
			$item->foreign_post = $item->highlight_id;
		}

		return response()->json($highlight_feed);
	}

	private static function easilyReadableDate($timestamp){
		$post_time = Carbon::parse($timestamp);

		$time_string = $post_time->format('n/j/y') . " at " . $post_time->format('g:ia');
		return $time_string;
	}

	private function addHighlightsMeta($feed, $local = true){
		foreach($feed as $post){
			$post = $this->getPosterData($post);

			$post->setAttribute('human_time', $this::easilyReadableDate($post->created_at));
			
			//Set the permalinks on all post type except RSS feed items since the originate from elsewhere outside of Juncture post items.
			if($post->service_origin !== 'rss'){
				$permalinks = $this::createHighlightPermaLink($post->highlight_id, $post->url_slug, $local);
				$post->setAttribute('permalink', $permalinks->permalink);
				$post->setAttribute('share_link', $permalinks->share_link);
			}
			
		}

		return $feed;
	}

	private function getPosterData($post){
		if(isset($post->foreign_server_id)){
			$post->setAttribute('poster_info', foreign_user::getUserInfo($post->foreign_server_id, $post->poster_id));
		}elseif($post->service_origin === 'rss'){
			$post->setAttribute('poster_info', rss_feed::getFeedInfo($post->poster_id));
		}else{
			$post->setAttribute('poster_info', User::public_info($post->poster_id));
		}

		return $post;
	}

	private static function createHighlightPermaLink($post_id, $slug, $local = true){
		$full_path = "highlight/" . $post_id . "/" . $slug;
		$share_link = env('APP_URL') . "highlight/" . $post_id;

		$permalink = "/" . $full_path;

		if(!$local){
			$permalink = env('APP_URL') . $full_path;
		}

		$permalinks = collect();

		$permalinks->permalink = $permalink;
		$permalinks->share_link = $share_link;

		return $permalinks;
	}
}