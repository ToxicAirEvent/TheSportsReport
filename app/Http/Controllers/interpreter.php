<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

use App\highlight;

class interpreter extends Controller
{

	private $embed_html_response;

	//An array of all currently supported media services for automatically parsing embeding.
	private $valid_sources = ["instagram","twitter","imgur","gfycat","youtube"];

	//Determines the media conent source and parses the embeding for that given source.
	public function mediaSource($source_type,$source_url){

		switch($source_type){
			case 'instagram':
				$embed_html_response = $this->instagram_embed($source_url);
				break;
			case 'youtube':
				$embed_html_response = $this->youtube_embed($source_url);
				break;
			case 'twitter':
				$embed_html_response = $this->twitter_embed($source_url);
				break;
			case 'streamable':
				$embed_html_response = $this->streamable_embed($source_url);
				break;
			case 'gfycat':
				$embed_html_response = $this->gfycat_embed($source_url);
				break;
			case 'imgur':
				$embed_html_response = $this->imgur_embed($source_url);
				break;
			case 'static':
				$embed_html_response = $this->static_embed($source_url);
				break;
			default:
				$embed_html_response = null;
		}

		$embed_preview_data = ['source_url' => $source_url,'source_site' => $source_type,'raw_embed_html' => $embed_html_response];

		return view('postPreview',['post_preview_data' => $embed_preview_data]);
	}


	/*
	 * This block of functions will contain various URL processors.
	 * Basically they will return html code that is embeded content from any various defined media service.
	*/
	private function instagram_embed($insta_url){
		return $this::messy_instagram_embed($insta_url);
	}

	private function twitter_embed($tweet_url){
		$build_url = 'https://publish.twitter.com/oembed?url=' . $tweet_url;

		$twitter_json_response = file_get_contents($build_url);

		$twitter_json_response = json_decode($twitter_json_response);

		return $twitter_json_response->html;
	}

	private function streamable_embed($streamable_url){
		$build_url = 'https://api.streamable.com/oembed.json?url=' . $streamable_url;

		$streamable_json_response = file_get_contents($build_url);

		$streamable_json_response = json_decode($streamable_json_response);

		/*
			Streamable attempts to embed videos at their natural size. We want to scale them down.
			This is hacky right now for 720 and 1080 resolution videos. In the future we'll want to do better with regex matching to probably set these scales dynamicall.
		*/
		$streamable_json_response->html = str_replace('width="1280"','width="100%"',$streamable_json_response->html);
		$streamable_json_response->html = str_replace('height="720"','height="550"',$streamable_json_response->html);

		$streamable_json_response->html = str_replace('width="1920"','width="100%"',$streamable_json_response->html);
		$streamable_json_response->html = str_replace('height="1080"','height="550"',$streamable_json_response->html);

		return $streamable_json_response->html;
	}

	private function youtube_embed($youtube_url){
		$strip_youtube_url = trim(substr($youtube_url, strpos($youtube_url, '?v=') + 3));

		$youtube_embed_url = 'https://www.youtube.com/embed/' . $strip_youtube_url;

		$youtube_formed_embed_code = '<iframe width="100%" height="450" style="max-height: 450px;" src="' . $youtube_embed_url . '" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>';

		return $youtube_formed_embed_code;
	}

	private function gfycat_embed($gfycat_url){
		$gfycat_url = rtrim($gfycat_url, '/');

		//gfycat doesn't have a set URL structure. Sometimes the feature detail strings. Other times its a straight resource. We have to account for this.
		if(strpos($gfycat_url, 'detail/') !== false){
			$strip_gfycat_url = trim(substr($gfycat_url, strpos($gfycat_url, 'detail/') + 7));
		}else{
			$strip_gfycat_url = substr($gfycat_url, strrpos($gfycat_url, '/' )+1);
		}

		$gfycat_embed_url = 'https://gfycat.com/ifr/' . $strip_gfycat_url;

		$gfycat_formed_embed_code = '<div style="position:relative;padding-bottom:54%"><iframe src="' . $gfycat_embed_url . '" frameborder="0" scrolling="no" width="100%" height="100%" style="position:absolute;top:0;left:0" allowfullscreen></iframe></div>';

		return $gfycat_formed_embed_code;
	}

	private function imgur_embed($imgur_url){
		$strip_imgur_url = trim(substr($imgur_url, strpos($imgur_url, 'imgur.com/') + 10));

		$imgur_formed_embed_code = '<blockquote class="imgur-embed-pub" lang="en" data-id="' . $strip_imgur_url . '"><a href="' . $imgur_url . '"></a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>';

		return $imgur_formed_embed_code;
	}

	private function static_embed($static_url){

		$imgur_formed_embed_code = '<img src="' . $static_url . '" class="img-fluid">';

		return $imgur_formed_embed_code;
	}


	/**
	  * Takes in data from the submitted form and previews it in the page.
	  * This allows us to make sure everything went ok when requesting or pasing embeding data before something is posted.
	  * @param Request
	  * @return view
	  **/
	public function preview_embeded_post(Request $preview_embed_content_request){
		
		$embeded_content_validation = Validator::make($preview_embed_content_request->all(), [
			'content_source'=>'required|in:instagram,twitter,imgur,gfycat,youtube,streamable,static',
			'content_url' => 'required|url',
		]);

		if ($embeded_content_validation->fails()) {
			return back()
				->withInput()
				->withErrors($embeded_content_validation);
		}

		return $this->mediaSource($preview_embed_content_request->content_source,$preview_embed_content_request->content_url);
	}

	/**
	  * Basically a duplicate of the above function. However this is used to load cached data from the web clipper extension.
	  * @param Request
	  * @return view
	  **/
	public function web_clipper_preview($cache_key){
		
		if (Cache::has($cache_key)) {
			$cached_preview = Cache::get($cache_key);
    		return $this->mediaSource($cached_preview['content_source'],$cached_preview['content_url']);
		}

		return redirect('home')->withErrors(['msg', 'Post cannot be found.']);
	}

	//After a highlight has been previewed we'll want to do a final validation on all its information and then insert it into the database.
	public function insert_new_highlight(Request $add_highlight_request){
		
		$embeded_content_validation = Validator::make($add_highlight_request->all(), [
			'content_origin_source'=>'required|in:instagram,twitter,imgur,gfycat,youtube,streamable,static',
			'content_origin_url' => 'required|url',
			'highlight_title' => 'required|string|min:3|max:240',
			'highlight_description' => 'nullable|string|max:5000',
			'content_embed_html' => 'required|url',
		]);

		if ($embeded_content_validation->fails()) {
			return redirect('/preview-post')
				->withInput()
				->withErrors($embeded_content_validation);
		}

		$embed_html_data = $this->mediaSource($add_highlight_request->content_origin_source,$add_highlight_request->content_embed_html);
		$embed_html_data = $embed_html_data->post_preview_data['raw_embed_html'];

		$slug_seo_url = $this->create_url_slug($add_highlight_request->highlight_title);

		highlight::insert([
			'highlight_title' => $add_highlight_request->highlight_title, 
			'origin_url' => $add_highlight_request->content_origin_url,
			'poster_id' => Auth::id(),
			'embed_data' => $embed_html_data,
			'highlight_description' => $add_highlight_request->highlight_description,
			'url_slug' => $slug_seo_url,
			'service_origin' => $add_highlight_request->content_origin_source,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		]);

		return redirect('/');
	}

	/**
	  *Deletes a highlight from the database based off the highlight ID
	  *	@param Request
	  *	@return mixed
	*/
	public function delete_highlight(Request $remove_highlight_request){

		//Make sure the highlight being requested for deletion exists in the database still.
		$remove_highlight_validation = Validator::make($remove_highlight_request->all(), [
			'highlight_unique_id'=>'required|exists:highlights,highlight_id',
		]);

		if ($remove_highlight_validation->fails()) {
			return back()
				->withInput()
				->withErrors($remove_highlight_validation);
		}

		$highlight_data = highlight::where('highlight_id','=',$remove_highlight_request->highlight_unique_id)->firstOrFail();

		/*
			Only admins should be allowed to delete foreign posts. As such if the foreign_post id field is set we'll need to check for admin rights.

			If that check passes we'll then do a comparison of if the user is an admin, or they are the user who made the post.
			If they are one of those they'll have the right to delete and we'll process it.

			Uses non-strict comparison operation because poster_id is stored as a string, at least returns as one from the database.
			This should potentially be fixed, but also might consider making user ids UUID4 which would become a string to string comparison.
		*/
		if(isset($highlight_data->foreign_post) && Auth::user()->admin < 1){
			return back()->withInput()->withErrors(["not_authorized" => "Only admins can delete foreign posts."]);
		}

		if(Auth::user()->id == $highlight_data->poster_id || Auth::user()->admin >= 1){
			highlight::where('highlight_id','=',$remove_highlight_request->highlight_unique_id)->delete();
			return redirect('/');
		}

		return back()->withInput()->withErrors(["not_authorized" => "You cannot delete someone elses post... Unless you're an admin."]);
	}

	protected function create_url_slug($title_or_string){
		//Honestly... Thanks someone on Stackoverflow for just doing all this regex to make friendly
		//https://stackoverflow.com/questions/11330480/strip-php-variable-replace-white-spaces-with-dashes#
		$title_or_string = strtolower($title_or_string);
		$title_or_string = preg_replace("/[^a-z0-9_\s-]/", "", $title_or_string);
		$title_or_string = preg_replace("/[\s-]+/", " ", $title_or_string);
		$title_or_string = preg_replace("/[\s_]/", "-", $title_or_string);

		return $title_or_string;
	}

	/*
		Instagram has a horrendous API which requires you authenticate with it if you want to so much as embed a post.
		This is a really messy way around that to just use the embed code they give you from within the website itself to make a html chunk for it.
	*/
	private static function messy_instagram_embed($insta_url){
		$insta_html = '<blockquote class="instagram-media" data-instgrm-permalink="' . $insta_url . '?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="14" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href=' . $insta_url . '?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;">View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="' . $insta_url . '?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared on Listening Station (@stlcitysc)</a></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>';

			return $insta_html;
	}
}
