<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rules\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Carbon\Carbon;

use App\highlight;

class videoManager extends Controller
{
    public function uploadVideo(Request $uploadRequest){

        //dd($uploadRequest);

        $validate_upload = Validator::make($uploadRequest->all(),[
            'video_file' => ['required',
                File::types(['avi','flv','mov','mp4','m4v'])->max(100000)
            ],
            'video_title' => 'required|string|min:3|max:240',
            'video_description' => 'nullable|string|max:5000'
        ]);

        if($validate_upload->fails()){
            return redirect()
            ->back()
            ->withInput()
            ->withErrors($validate_upload);
        }

        $file_key = time() . "_" . Str::random(24);

        $rnd_vid_name = $file_key . '.' . $uploadRequest->file('video_file')->getClientOriginalExtension();

        /*
            To actually write the file to the disk path we have to use the StoreAs function to access the server directory.
            This will give you something close to the full path and save the file correctly. However it will leave off any directory prefixes the filesystem has.

            So 'video_uploads' won't return the '/videos/' prefix to the path.
            This is why we then make "http_path" to save the relative path to the video in the database.
        */
        $filePath = $uploadRequest->file('video_file')->storeAs('raw_uploads', $rnd_vid_name, 'video_uploads');
        $http_path = "/videos/" . $filePath;

        highlight::insert([
            'highlight_title' => $uploadRequest->video_title,
            'highlight_description' => $uploadRequest->video_description,
            'poster_id' => Auth::id(),
            'origin_url' => $http_path,
            'url_slug' => $file_key,
            'service_origin' => 'organic_video',
            'created_at' => Carbon::now()->toDateTimeString(),
        ]);

        return redirect('/');
    }
}
