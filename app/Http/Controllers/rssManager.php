<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

use App\rss_feed;
use App\highlight;

use Feeds;

class rssManager extends Controller
{
    public function rssFeedList(){
        $rss_feed_sites = rss_feed::all();

        return view('rssFeeds.rssFeedList', ['rss_feed_list' => $rss_feed_sites]);
    }

    public function singleFeedInfo($feed_id){
        $feed_info = rss_feed::where('id', '=', $feed_id)->firstOrFail();
        $feed_posts = highlight::where('poster_id' , '=', $feed_id)
                                ->take(25)
                                ->orderby('created_at','DESC')
                                ->get();

        return view('rssFeeds.singleRssFeed', ['feed_info' => $feed_info, 'recent_posts' => $feed_posts]);
    }

    public function newRssFeed(){
        return view('rssFeeds.newRssFeed');
    }

    public function processNewFeed(Request $new_feed_req){
        $validate_new_feed = Validator::make($new_feed_req->all(), [
            'feed_title'=>'required|string|max:190',
            'feed_url' => 'required|url|unique:rss_feeds,feed_url|max:190',
            'feed_homepage' => 'required|url|max:190',
        ]);

        if($validate_new_feed->fails()) {
            return back()
                ->withInput()
                ->withErrors($validate_new_feed);
        }

        $new_feed = new rss_feed;
        $new_feed->id = 'rss_' . Str::uuid();
        $new_feed->feed_title = $new_feed_req->feed_title;
        $new_feed->feed_homepage = $new_feed_req->feed_homepage;
        $new_feed->feed_url = $new_feed_req->feed_url;
        $new_feed->save();

        $created_feed = rss_feed::orderBy('created_at','desc')->firstOrFail();

        return redirect('/admin/rss-feeds/edit/' . $created_feed->id);
    }

    public function updateFeedInfo(Request $update_feed_req){
        $validate_new_feed = Validator::make($update_feed_req->all(), [
            'feed_id' => 'required|exists:rss_feeds,id',
            'feed_title'=>'required|string|max:190',
            'feed_url' => [
                'required',
                'url',
                Rule::unique('rss_feeds', 'feed_url')->ignore($update_feed_req->feed_id)->where(function ($query) use ($update_feed_req) {
                    $query->where('feed_url', $update_feed_req->feed_url);
                })
            ],
            'feed_homepage' => 'required|url|max:190',
        ]);

        if($validate_new_feed->fails()) {
            return back()
                ->withInput()
                ->withErrors($validate_new_feed);
        }

        $update_feed = rss_feed::find($update_feed_req->feed_id);
        $update_feed->feed_title = $update_feed_req->feed_title;
        $update_feed->feed_homepage = $update_feed_req->feed_homepage;
        $update_feed->feed_url = $update_feed_req->feed_url;
        $update_feed->save();

        return redirect()->back();
    }
}
