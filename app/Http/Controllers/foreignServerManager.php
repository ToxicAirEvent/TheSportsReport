<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

use App\foreign_server;
use App\highlight;

class foreignServerManager extends Controller
{
    public function foreignServerList(){
        $server_list = foreign_server::all();

        foreach($server_list as $server){
            $parse_endpoint = parse_url($server->endpoint);

            $tld_string = $parse_endpoint['host'] . "/";

            $server->tld = $tld_string;
        }

        return view('foreignServers.foreignServerList', ['foreign_servers' => $server_list]);
    }

    public function singleServerInfo($server_id){
        $server_info = foreign_server::where('id', '=', $server_id)->firstOrFail();
        $server_highlights = highlight::where('foreign_server_id' , '=', $server_id)
                                ->take(round($server_info->store_count * 1.2))
                                ->orderby('created_at','DESC')
                                ->get();

        return view('foreignServers.singleServerInfo', ['server_info' => $server_info, 'recent_highlights' => $server_highlights]);
    }

    public function newForeignServer(){
        return view('foreignServers.newForeignServer');
    }

    public function processNewServer(Request $new_server_req){
        $validate_new_server = Validator::make($new_server_req->all(), [
            'server_name'=>'required|string|max:190',
            'endpoint' => 'required|url|unique:foreign_servers,endpoint',
            'store_count' => 'required|min:1|max:100',
        ]);

        if($validate_new_server->fails()) {
            return back()
                ->withInput()
                ->withErrors($validate_new_server);
        }

        $new_server = new foreign_server;
        $new_server->id = Str::uuid();
        $new_server->server_name = $new_server_req->server_name;
        $new_server->endpoint = $new_server_req->endpoint;
        $new_server->store_count = $new_server_req->store_count;
        $new_server->save();

        $created_server = foreign_server::orderBy('created_at','desc')->firstOrFail();

        return redirect('/admin/edit-server/' . $created_server->id);
    }

    public function processServerUpdate(Request $update_server_req){
        $validate_update_server = Validator::make($update_server_req->all(), [
            'server_id' => 'required|exists:foreign_servers,id',
            'server_name'=>'required|string|max:190',
            'endpoint' => ['required','url', 
                Rule::unique('foreign_servers', 'endpoint')->ignore($update_server_req->server_id)->where(function ($query) use ($update_server_req) {
                    $query->where('endpoint', $update_server_req->endpoint);
                })
            ],
            'store_count' => 'required|min:1|max:100',
        ]);

        if($validate_update_server->fails()) {
            return back()
                ->withInput()
                ->withErrors($validate_update_server);
        }

        //dd($update_server_req);

        $update_server = foreign_server::find($update_server_req->server_id);
        $update_server->server_name = $update_server_req->server_name;
        $update_server->endpoint = $update_server_req->endpoint;
        $update_server->store_count = $update_server_req->store_count;
        $update_server->save();

        return redirect()->back();
    }
}
