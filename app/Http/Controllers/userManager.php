<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rules\File;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

use App\User;

use Carbon\carbon;
use Image_mod;

class userManager extends Controller
{
    public function listUsers(){
        $all_users = User::all(['id','email','name','admin','poster','created_at','updated_at']);

        return view('admin.user_list',['users' => $all_users]);
    }

    public function getUser($user_id){

        $user_info = User::where('id','=',$user_id)->firstOrFail(['id','email','name','admin','poster','created_at','updated_at']);

        return view('admin.edit_user',['user' => $user_info]);
    }

    public function updateUser(Request $userUpdate){
        $valid_update = Validator::make($userUpdate->all(),[
            'user_id' => 'required|integer|exists:users,id',
            'name' => 'required|string|max:75',
            'email' => 'required|email|max:190',
            'site_admin' => 'integer|max:256',
            'approved_poster' => 'boolean',
        ]);

        if($valid_update->fails()){
            return redirect()
            ->back()
            ->withInput()
            ->withErrors($valid_update);
        }

        $update_user = User::find($userUpdate->user_id);

        $update_user->name = $userUpdate->name;
        $update_user->email = $userUpdate->email;

        //If no data is passed in for the admin status just set it to false since check checkbox wasn't hit.
        if(!isset($userUpdate->site_admin)){
            $update_user->admin = 0;
        }else{
            $update_user->admin = $userUpdate->site_admin;
        }

        //If no data is passed in for the poster status just set it to false since check checkbox wasn't hit.
        if(!isset($userUpdate->approved_poster)){
            $update_user->poster = 0;
        }else{
            $update_user->poster = $userUpdate->approved_poster;
        }

        $update_user->save();

        return redirect()->back()->with('success','User has been updated!');
    }

    public function addAvatar(Request $avatarUpload){
        $validate_image = Validator::make($avatarUpload->all(),[
            'avatar_file' => ['required',
                File::image()->max(2048)->dimensions(Rule::dimensions()->minHeight(32)->minHeight(32))
            ]
        ]);

        if($validate_image->fails()){
            return redirect()
            ->back()
            ->withInput()
            ->withErrors($validate_image);
        }

        $avatar = Image_mod::make($avatarUpload->file('avatar_file'));
        $avatar->fit(192,192, function($constraint){
            $constraint->upsize();
        });

        $avatat_name = "uid-" . Auth::id() . "_" . time() . "_" . Str::random(10) . ".jpg";
        $avatar_file = Storage::disk('avatars')->path('') . $avatat_name;

        $avatar->save($avatar_file);

        $update_user = User::find(Auth::id());
        $update_user->avatar_url = $avatat_name;
        $update_user->save();


        return redirect()->back()->with('success','You are the proud owner of a shiny new avatar!');
    }
}
