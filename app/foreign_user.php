<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class foreign_user extends Model
{
    use HasFactory;

    protected $guarded = [];

    public $incrementing = false;

    protected function getUserInfo($server_id, $user_id){
        $the_user = $this::where('foreign_server_id', '=', $server_id)->where('id', '=', $user_id)->first();

        return $the_user;
    }
}
