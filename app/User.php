<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected function public_info($user_id){
        $the_user = $this->where('id','=',$user_id)->first(['id','name','admin','poster','avatar_url']);

        /*
            Referencing an image on an object that may or may not exist and needs to go to external sites was difficult to handle every time it was needed.
            So the easier thing to do is to build the entire path to the avatar image every time instead of trying to make up valid relative paths on the fly.
        */
        if(isset($the_user)){
            if(isset($the_user->avatar_url)){
                $the_user->avatar_url = env('APP_URL') . "profiles/avatars/" . $the_user->avatar_url;
            }else{
                $the_user->avatar_url = env('APP_URL') . "profiles/avatars/default.png";
            }
        }

        return $the_user;
    }
}
