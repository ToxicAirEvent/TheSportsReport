<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class job extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $primaryKey = 'job_title';
    protected $keyType = 'string';
}
