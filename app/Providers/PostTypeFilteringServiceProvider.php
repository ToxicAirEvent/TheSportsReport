<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

use App\highlight;

class PostTypeFilteringServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $post_count_by_type = null;

        if(Cache::has('post_types_count')){
            $post_count_by_type = Cache::get('post_types_count');
        }else{
            $post_count_by_type = $this->getAndCountPostTypes();
        }

        //Getting route parameters on a provider proves to be tricky but you can use this handy route builder at the current URI to reverse engineer it.
        $request = app(Request::class);
        if($request->isMethod('get')){
            $parameters = $this->app->router->getRoutes()->match($this->app->request->create($this->app->request->getRequestUri()))->parameters();
        }

        $current_mode = null;
        $current_types = null;
        $applied_filters = [];

        if(isset($parameters['type_list'])){
            $current_types = $parameters['type_list'] . ",";
            $applied_filters = explode(",",$current_types);
        }

        if(isset($parameters['mode'])){
            $current_mode = $parameters['mode'];
        }

        view()->composer('filters.postFilters', function ($view) use ($post_count_by_type, $current_mode, $current_types, $applied_filters) {
            $view->with([
                'post_types' => $post_count_by_type,
                'set_mode' => $current_mode,
                'set_types' => $current_types,
                'applied_filters' => $applied_filters,
            ]);
        });
    }

    private function getAndCountPostTypes(){
        $highlight_list = highlight::select(['service_origin'])->get();

        $highlight_type_count = $highlight_list->countBy('service_origin');

        Cache::put('post_types_count', $highlight_type_count, 21600);

        return $highlight_type_count;
    }
}
