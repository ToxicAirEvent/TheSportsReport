<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->post('/web_clip','extensionAPI@cacheWebClip');

Route::group(['middleware' => 'throttle:15,5'], function () {
	Route::get('recent-posts/{last?}','highlightFeed@apiFeed');
});
