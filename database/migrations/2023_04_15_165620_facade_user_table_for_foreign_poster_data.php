<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        DB::statement('SET SESSION sql_require_primary_key=0');

        Schema::create('foreign_users', function (Blueprint $table) {
            $table->foreignUuid('foreign_server_id');
            $table->unsignedBigInteger('id');
            $table->char('name', 76);
            $table->string('avatar_url')->nullable();
            $table->tinyInteger('admin')->default(0);
            $table->tinyInteger('poster')->default(0);
            $table->timestamps();

            $table->primary(['foreign_server_id', 'id']);
        });

        DB::statement('SET SESSION sql_require_primary_key=1');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('foreign_users');
    }
};
