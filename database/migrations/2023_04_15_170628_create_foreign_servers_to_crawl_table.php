<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        DB::statement('SET SESSION sql_require_primary_key=0');

        Schema::create('foreign_servers', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('server_name');
            $table->string('endpoint');
            $table->smallInteger('store_count')->default(25);
            $table->timestamps();
        });

        DB::statement('SET SESSION sql_require_primary_key=1');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('foreign_servers');
    }
};
