<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        DB::statement('SET SESSION sql_require_primary_key=0');

        Schema::create('rss_feeds', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('feed_title', 120);
            $table->string('feed_homepage');
            $table->string('feed_url');
            $table->timestamp('last_crawled')->nullable();
            $table->timestamps();
            $table->unique('id');
        });

        DB::statement('SET SESSION sql_require_primary_key=0');
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rss_feeds');
    }
};
