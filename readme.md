
# Listening Station - Decentralized, Distributed Social and News Feeds
## Alpha Release - June 4th, 2023

Listening Station acts as a platform for social posting, web bookmarking, and RSS feed consolidation that can be setup and run by nearly anyone with a web server that supports PHP and mySQL.

Any server operator can set up a station (node) and post whatever they like to their station. Stations make an API feed of their recent post available. This allows other stations to act as listeners (crawlers) to each other. A station is capable of importing recently broadcast feed items on other stations into their station. Thus creating a web of listening stations that share posts between each other.

Each station operator controls which stations their station is capable of listening to. Meaning a station can be curated to whatever an operator wants to see.

## Core Features
There are 3 main components to Listening Station servers that all other functionality is built off of and on top of.
* Web bookmarking - There is a class/controller known as the 'interpreter' which takes in URL strings submitted to it (Twitter, Instagram, Imgur, etc) and embeds them to the stations feed. This allows for inline viewing of content from many different websites in one scrollable feed.
* Organic Posting - Users are capable of uploading videos (and soon to be photos, and test posts) to the Station. This allows for quick sharing of content to others via a publicly accessible website where station administrators have full control over their content.
* RSS Feed Aggregation - Each listening station can be configured to periodically crawl any RSS feed that the station operator wishes. RSS items are converted into feed posts on the station and intermixed alongside the web bookmarks and organic posts from other users on the station.

Note: RSS feed items do not carry forward. Meaning if your station is listening to other stations to import their posts only web bookmarks and organic posts will be imported. RSS feed are ignored.

# Hosting A Station

One of the goals of this system is to make it as easy as possible for people to get installed and running. Some level of technical knowhow is always going to be required. Currently this is an alpha project though so setup may be extra difficult, and something you should do at your own risk.

If you are interested in hosting a server but having trouble setting it up. Please get in touch.

## Installing A Station

Listening Station is built off of [Laravel](https://laravel.com/). As such any server with PHP, Composer, and a mySQL database should be able to run it.

### Step 1:

Clone this repo or upload it to your server.

`git clone https://gitlab.com/ToxicAirEvent/TheSportsReport.git ./`

### Step 2:

Configure a mySQL database for your station.

### Step 3:

Open up the `.env.example` file and update the information within it to match what you want the settings for your server to be.

Some ones to keep an eye on...

* `APPROVED_POSTERS_ONLY = 1` By default anyone can register for your station. The default state of users who register is set to not being approved to make post on your station. This is because I've yet to implement a better system for user and email verificaiton to prevent spam. if you want to open up the fire hose to whatever anyone wants to post set this to 0.
* `POST_PER_PAGE = 25` This is the default amount of posts shown per page of the homepage feed. You can make it show more post or less. The app should easily be able to show more as it simply orders posts by date/time created without too many pivots/lookups taking place to build the feed. However do know some take place under the hood so it can slow things down.
* All `APP_` flags should be configured for your server and what you want to call your station.
* All `DB_` flags should be configured to connect to the mySQL server you have set up.

All the other parameters are native Laravel flags. The email and caching functionality is not yet robustly built out for this app. As such these settings can be ignored for now unless you want to dig in yourself and set up an email server or modifying caching methodologies.

### Step 4: Install FFMPEG

FFMPEG is used under the hood to convert, compress, and optimize videos posted to your station for web viewing.

You'll want to install it to allow this functionality to work correctly. If you do not wish to optimize videos because it uses a lot of server overhead that is fine. Not installing ffmpeg should allow that task to fail gracefully without impact otherwise.

Consult the [FFMPEG docs](https://ffmpeg.org/download.html) to determine how to install it for your operating system.

This should require no config from you. All the optimization and command line settings are built into the app otherwise.

### Step 5: Install Laravel and other packages

This should be as simple as running `composer install` in the directory you cloned your repo into.

Depending on the power of the server or PC you are installing the app on this process might take a bit.

### Step 6: Migrate and populate database structures

Laravel easily allows database structures to be mapped to any install of an app for quick use.

In your console run `php artisan migrate` and wait for the database to be configured.

### Step 7: Website is up. Make yourself an admin

Navigate to your website, register for an account.

Once you've registered your account in your console for your server run the command `php artisan admin:first-user`

This will make your account an admin for your station. Which will allow you to access the `/admin` route of the station and manage it.

### Step 8: Get your CRON task running

Listening Station relies on CRON task running in the background to get your feeds imported, and optimize videos.

How you set the CRON tab for your server will vary. If you've got true CLI access you should be able to set it via `crontab -e`

However other web hosts allow you to manage CRON tasks via a web GUI or other means.

Regardless the basic CRON tab line you'll want to install is...

`* * * * * cd /path/to/your/ListeningStation && php artisan schedule:run >> /home/[your_user_name]/listening_station_cron.log`

# Updating The App
Since the app is cloned in from the public repo updating is very easy. You'll just need to get the updated version of the master branch.

### Step 1: Pull
Run `git pull` to retrieve the latest updates to the master branch/release version of Listening Station.

If git doesn't know what branch you're on switch to master with `git checkout master` and try the pull again.

### Step 2: Migrate

Some Updates will make changes to the database structure. These are all handled through Laravel migrations. So you will be able to apply them all with one command.

Run `php artisan migrate` in your station/apps directory and let it process.

### Step 3: Update Packages

Sometimes there will be package or provider updates to the app which requires you to install or update them via Composer.
This is done with a single command of `composer update`

### Step 4: Done.

App should now be up and running again with the latest features and tweaks deployed.

# Known Issues:
* Share links on organic videos and optimized videos are broken. It adds an extra `/videos/` part to the URL path.
* On some servers the cron job will not run unless you have it output to a log file as is suggested in step 8. Ideally you should be able to ignore this.
* Special character encoding is difficult to parse on some RSS feeds which will result in mangled post titles.
* User uploading a new avatar image will create a new image on the server instead of just replacing the previous one. This can cause storage bloat.

# Features On The Way:
* Add support for embeding TikTok videos
* Add embed support for Twich.tv clips
* Add support for uploading your own images
* Add support for user created text posts
* Post and story blacklisting. If a single post from a server you listen to isn't appropriate or is causing issues on your server, you should be able to delete it without it coming back next crawl.
* Create a better system for new user sign-ups. Be that easier to get running email verification or a dashboard where admins can add new users, or both.
* Add support to have multiple feeds and the ability to sort so only certain types of posts are seen at any given time.
* Add automatic tagging system for news feed or foreign server posts to help with sorting and filtering
* Add commenting on posts so people can discuss topics or posts. Not just share content.

# Quality Of Life
* Remove Gfycat embed support (Service was/is shut down)
* Add a `.env` flag to disable video compression if people wish to not have that run as part of their server overhead.
* Put visual indicators on posts from other that are imported from foreign servers.
* Create a better new post dashboard that so that it is easier to tell what you're doing.

# Recently Fixed or Added
* Base64 `APP_KEY` key field is now empty in `.env.example` so that installs will complete successfully.
* Flagging updates on migrations for tables without primary keys to allow them to fully complete on some server hosts.
* Video upload directories will be automatically created and not ignore when you pull in and install the repo for the first time. Allowing permissions to be correctly set on these folders. (Fixed: 6/11/2023)
* Made permission logic changes so that only admins can delete any posts. Users who made a post locally can now delete their own posts. This same logic also applies to tagging. (Fixed: 6/11/2023)
* Using the sidebar on the homepage of the site you can now filter posts in or out by type and source. (Added: 6/11/2023)

# License
Currently this software is intended for personal use only. It should not be repackaged, or redestributed without the authors express, written consent. (Revised 6/4/2023)

# Warranty
Software is provided as is, and without warranty, and no guarantees to the safety and security of your platform, data, or devices. You assume all risk by using it.