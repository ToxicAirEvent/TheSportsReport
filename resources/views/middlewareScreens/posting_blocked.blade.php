@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Approved Posters Only</h1>
            <p>This website only allowed approved users to make posts.</p>
            <p>You'll need to contact an admin to get your account setup for posting access.</p>
            <p><small>Users with the admin status on their account don't need to be approved for posting.</small></p>
        </div>
    </div>
</div>
@endsection
