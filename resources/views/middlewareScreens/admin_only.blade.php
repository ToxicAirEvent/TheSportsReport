@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Admin Access Only</h1>
            <p>The following page can only be accessed by admins. Which you are not.</p>
            <p>Please reach out to one of the existing admins to request access if you think you deserve it.</p>
        </div>
    </div>
</div>
@endsection
