@if(Session::has('success'))
    <div class="alert alert-success">
        <strong>WHOOOO!</strong>

        <br><br>

        <p>{{Session::get('success')}}</p>
    </div>
@endif