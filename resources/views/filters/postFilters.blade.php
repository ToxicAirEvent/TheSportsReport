<h3>Filter Out:</h3>

@if(count($applied_filters) >= 1)
	<p><a href="/">Clear Filters [Return Home]</a></p>
@endif

<ul>
@foreach($post_types as $type => $count)
	@include('filters.contextualListItem', [
		'type' => $type, 
		'applied_filters' => $applied_filters, 
		'set_mode' => $set_mode,
		'list_mode' => 'out'
	])
@endforeach
</ul>

<h3>View Only:</h3>
<ul>
@foreach($post_types as $type => $count)
	@include('filters.contextualListItem', [
		'type' => $type, 
		'applied_filters' => $applied_filters, 
		'set_mode' => $set_mode,
		'list_mode' => 'only'
	])
@endforeach
</ul>