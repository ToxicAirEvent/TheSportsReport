@if(in_array($type, $applied_filters) && $set_mode === $list_mode)
	<li>{{$type}} ({{$count}})</li>
@elseif($set_mode !== $list_mode)
	<li><a href="/filter/{{$list_mode}}/{{$type}}">{{$type}} ({{$count}})</a></li>
@else
	<li><a href="/filter/{{$list_mode}}/{{$set_types}}{{$type}}">{{$type}} ({{$count}})</a></li>
@endif