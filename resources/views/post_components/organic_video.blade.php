<p class="text-muted">
	Uploaded Video:
	<a href="/videos/{{$vid->origin_url}}" download>Original</a>
	@if($vid->optimized_file !== null) | <a href="/videos/{{$vid->optimized_file}}" download>Compressed</a>@endif
</p>