<div class="poster-info mtm mbm">
	@php
		$user_av = "/profiles/avatars/default.png";

		if(isset($poster->avatar_url)){
			$user_av = $poster->avatar_url;
		}

	@endphp

	<span class="user-avatar" style="background-image: url('{{$user_av}}');"></span>
	@if($poster !== null)
		<span>{{$poster->name}}</span>

		@if($post_type === 'rss')
			<span class="feed-notice">[Feed]</span>
		@endif
	@else
		<span class="text-muted">¯\_(ツ)_/¯ (Unknown Poster)</span>
	@endif

	on {{$post_time}}
</div>
