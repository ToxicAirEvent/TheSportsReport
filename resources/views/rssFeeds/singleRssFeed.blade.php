@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		@include('common.errors')

		@include('rssFeeds.components.updateRssFeed',['feed' => $feed_info])
	</div>

	<div class="row">
		<div class="col-12">
			<h1><a href="$feed_info->feed_homepage" target="_blank">{{$feed_info->feed_title}}</a></h1>
			<p>Feed URL: <a href="{{$feed_info->feed_url}}" target="_blank">{{$feed_info->feed_url}}</a></p>
			
			@foreach($recent_posts as $post)
				<p><a href="{{$post->permalink}}" target="_blank">{{$post->highlight_title}}</a> | {{$post->created_at}}</p>
			@endforeach
		</div>
	</div>
</div>
@endsection