<div class="col-12">
	<h2>Edit Feed Properties</h2>

	@include('common.errors')
	
	<form action="/admin/rss-feeds/update-feed" method="POST">
	    {{ csrf_field() }}

	    <input type="hidden" id="feed_id" name="feed_id" value="{{$feed->id}}">

	    <div>
	        <label for="feed_title">Feed Name:</label>
	        <input type="text" id="feed_title" name="feed_title" maxlength="240" size="50" value="{{$feed->feed_title}}" required>
	    </div>

	    <div>
	        <label for="feed_homepage">Feed Homepage URL:</label>
	        <input type="text" id="feed_homepage" name="feed_homepage" maxlength="2048" size="50" value="{{$feed->feed_homepage}}" required>
	    </div>

	    <div>
	        <label for="feed_url">Feed URL:</label>
	        <input type="text" id="feed_url" name="feed_url" max="2048" value="{{$feed->feed_url}}" required>
	    </div>

	    <button type="submit">Update RSS Feed</button>
	</form>
</div>