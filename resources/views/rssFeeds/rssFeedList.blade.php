@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1>Crawled RSS Feeds</h1>
			<p>Juncture is capable of reading RSS feeds from other websites and populating them into the feed on your website.</p>

			<p>Feeds are crawled every 15 minutes and feed items not already posted to your instance will be added.</p>

			<p>
				Feeds are not carried forward by default.
				Meaning RSS posts on your juncture instance aren't reported/included in the API feed your site broadcasts out.
				Additionally other servers can flag their API endpoint URLs to not accept RSS feed items from your site if they so choose.
				Flag is ?rssReject=true
			</p>
		</div>
	</div>

	<div class="row">
		@foreach($rss_feed_list as $feed)
			<div class="col-12">
				<h4>{{$feed->feed_title}} / <a href="{{$feed->feed_homepage}}" target="_blank">{{$feed->feed_homepage}}</a></h4>
				<p>
					ID: {{$feed->id}} | Endpoint: <a href="{{$feed->feed_ur}}" target="_blank">{{$feed->feed_url}}</a> | Last Crawled: {{$feed->last_crawled}}
				</p>

				<p>[<a href="/admin/rss-feeds/edit/{{$feed->id}}">Edit</a>]</p>
				<hr>
			</div>
		@endforeach
	</div>
</div>
@endsection