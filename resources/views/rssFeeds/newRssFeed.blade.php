@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1>Add New RSS Feed</h1>

			@include('common.errors')
			
			<form action="/admin/rss-feeds/add-new" method="POST">
			    {{ csrf_field() }}

			    <div>
			        <label for="feed_title">Feed Name:</label>
			        <input type="text" id="feed_title" name="feed_title" maxlength="240" size="50" required>
			    </div>

			    <div>
			        <label for="feed_homepage">Feed Homepage URL:</label>
			        <input type="text" id="feed_homepage" name="feed_homepage" maxlength="2048" size="50" required>
			    </div>

			    <div>
			        <label for="feed_url">Feed URL:</label>
			        <input type="text" id="feed_url" name="feed_url" max="2048" required>
			    </div>

			    <button type="submit">Add RSS Feed</button>
			</form>
		</div>
	</div>
</div>
@endsection