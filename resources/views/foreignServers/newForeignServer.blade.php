@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1>Add New Foreign Server</h1>

			@include('common.errors')
			
			<form action="/admin/foreign-servers/add-new" method="POST">
			    {{ csrf_field() }}

			    <div>
			        <label for="server_name">Server Name:</label>
			        <input type="text" id="server_name" name="server_name" maxlength="191" size="50" required>
			    </div>

			    <div>
			        <label for="endpoint">Endpoint:</label>
			        <input type="text" id="endpoint" name="endpoint" maxlength="191" size="50" required>
			    </div>

			    <div>
			        <label for="store_count">Store Count:</label>
			        <input type="number" id="store_count" name="store_count" min="1" max="100" value="25">
			    </div>

			    <button type="submit">Add Server</button>
			</form>
		</div>
	</div>
</div>
@endsection