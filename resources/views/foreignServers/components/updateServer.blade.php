<div class="container">
	<div class="row">
		<div class="col-12">
			<h1>Add New Foreign Server</h1>
			
			<form action="/admin/foreign-servers/update-server" method="POST">
			    {{ csrf_field() }}

			    <input type="hidden" id="server_id" name="server_id" value="{{$server->id}}">

			    <div>
			        <label for="server_name">Server Name:</label>
			        <input type="text" id="server_name" name="server_name" maxlength="191" size="50" value="{{$server->server_name}}" required>
			    </div>

			    <div>
			        <label for="endpoint">Endpoint:</label>
			        <input type="text" id="endpoint" name="endpoint" maxlength="191" size="50" value="{{$server->endpoint}}" required>
			    </div>

			    <div>
			        <label for="store_count">Store Count:</label>
			        <input type="number" id="store_count" name="store_count" min="1" max="100" value="{{$server->store_count}}">
			    </div>

			    <button type="submit">Update Server</button>
			</form>
		</div>
	</div>
</div>