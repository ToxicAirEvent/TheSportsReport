@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		@include('common.errors')
		
		@include('foreignServers.components.updateServer', ['server' => $server_info])
	</div>

	<div class="row">
		<div class="col-12">
			<h1>{{$server_info->server_name}}</h1>
			
			@foreach($recent_highlights as $highlight)
				<p><a href="{{$highlight->permalink}}" target="_blank">{{$highlight->highlight_title}}</a></p>
			@endforeach
		</div>
	</div>
</div>
@endsection