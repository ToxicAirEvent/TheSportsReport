@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1>Foreign Servers</h1>
			<p>Post can be pulled in from other servers you find interesting by using the <em>foreign server</em> functionality of your Juncture instance.</p>
			<p>By default each Juncture server launches with public api end point which servers up the 20 most recent post of that server.</p>
			<p>
				As such any server you know of and find to have interesting posts can be referenced onto your server.
				This allows you to build a single dashboard for your server that is a mix of interesting people and posts from all around the web if you so choose.
			</p>

			<p>
				It should also be noted that foreign posts are only allowed to make one "jump."
				Meaning only first party post are broadcast out to other servers from your server or the servers you follow.
				So Server A following Server B, and then Server C following Server A will not recieve the posts from Server B. And so on.
			</p>
		</div>
	</div>

	<div class="row">
		@foreach($foreign_servers as $server)
			<div class="col-12">
				<h4>{{$server->server_name}} / <a href="//{{$server->tld}}" target="_blank">{{$server->tld}}</a></h4>
				<p>
					ID: {{$server->id}} | Endpoint: <a href="{{$server->endpoint}}" target="_blank">{{$server->endpoint}}</a> | Post Cap: {{$server->store_count}}
				</p>

				<p>[<a href="/admin/edit-server/{{$server->id}}">Edit</a>]</p>
				<hr>
			</div>
		@endforeach
	</div>
</div>
@endsection