@extends('layouts.app')

@section('content')
<div class="container">

	<div class="row">
		<div class="col-12">
			<h1>Add A New Post</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			@include('common.errors')
		</div>
	</div>

	<div class="row ptl">
		<div class="col-12">
			<h1>Upload Video</h1>
		</div>

		<div class="col-12">
			<div class="panel-body">
				<form action="/video/upload-video" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}

					<p>
						<strong>Title Your Highlight:</strong> <input type="text" name="video_title" class="formWidth">
						<strong class="text-danger">required</strong>
					</p>

					<input type="file" name="video_file">

					<p>
						<strong>Description It:</strong>
						<textarea rows="4" cols="50" name="highlight_description" id="video_description" class="formWidth"></textarea>
						<em class="text-info">Or don't. We don't care.</em>
					</p>

					<input type="submit" value="Upload It!">
				</form>
			</div>
		</div>
	</div>

	<div class="row ptl">

		<div class="col-12">
			<h1>Embed Content From Another Site</h1>
		</div>

		<div class="panel-body">
			<!-- Preview a post of embeded content -->
			<form action="/preview-post-output" autocomplete="off" method="POST" class="form-horizontal">
				{{ csrf_field() }}

				<!-- Edit Source Info Fields -->
					<div class="col-xs-12 col-md-12">
						<p><strong>Post Source:</strong></p>

						<select name="content_source">
						  <option value="instagram">instagram</option>
						  <option value="youtube">YouTube</option>
						  <option value="twitter">Twitter</option>
						  <option value="streamable">Streamable</option>
						  <option value="gfycat">Gfycat</option>
						  <option value="imgur">imgur</option>
						  <option value="static">Static Image</option>
						</select>
					</div>
				
					<div class="clear"></div>

					<div class="col-xs-12 col-md-10">
						<p><strong>Content URL:</strong> <input type="text" name="content_url" id="content_url" class="formWidth"></p>
					</div>
				
				<!-- Update Source Button -->
				<div class="form-group">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<button type="submit" class="btn btn-default cursor-pointer">
							<i class="fa fa-plus"></i> Preview Embeded Content
						</button>
					</div>
				</div>
			</form>
			<!-- End Preview a post of embeded content -->
		</div>
	</div>
</div>
@endsection