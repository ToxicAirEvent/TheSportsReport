@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Admin Dashboard</h1>
            <p>It is a work in progress but it'll get you around.</p>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <h2>Manage Users</h2>
            <p><a href="/admin/manage-users">User List and Flags</a></p>
            <p>You can see permissions per user there and edit specific users.</p>

            <h2>Manage Foreign Servers</h2>
            <p><a href="/admin/foreign-servers">View All Foreign Servers</a></p>
            <p><a href="admin/foreign-servers/new">Add New Foreign Server</a></p>

            <h2>RSS Feed Manager</h2>
            <p><a href="/admin/rss-feeds/view-all">View All RSS Feeds</a></p>
            <p><a href="/admin/rss-feeds/new">Add New RSS Feed</a></p>
        </div>
    </div>
</div>
@endsection
