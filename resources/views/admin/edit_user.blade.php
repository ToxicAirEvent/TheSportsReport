@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Editing {{$user->name}}</h1>
            <p>Make changes to this user in the form below or <a href="/admin/manage-users">go back to all users.</a></p>
        </div>
    </div>

    <div class="row">
        <div class="col-12">

            @include('common.errors')
            @include('common.success')

            <form method="POST" action="/admin/update-user">

                {{ csrf_field() }}
                <input type="hidden" name="user_id" value="{{$user->id}}">

                <label for="name">Username:</label><input type="text" name="name" value="{{$user->name}}">
                <label for="email">Email:</label><input type="text" name="email" value="{{$user->email}}">

                <div>
                    <input type="checkbox" name="approved_poster" value="1" @if($user->poster === 1) checked @endif>
                    <label for="approved_poster">Approved Poster</label>
                </div>

                <div>
                    <input type="checkbox" name="site_admin" value="1" @if($user->admin === 1) checked @endif>
                    <label for="site_admin">Site Admin</label>
                </div>

                <input type="submit" value="Update User">
            </form>
        </div>
    </div>
</div>
@endsection
