@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>User Manager</h1>
            <p>A view into the status of all users on the website and what permissions they have regarding admin access and posting ability.</p>
            <p>
                <strong>Key:</strong>
                Flags with <span class="text-success">green text</span> means the user has access to the permission.
                While <span class="text-danger">red text</span> means that the user does not have access to the permission.
            </p>
        </div>
    </div>

    <div class="row">
        @foreach($users as $user)
            <div class="col-12">
                <p><b>{{$user->name}}</b> (Email: {{$user->email}})</p>
                <ul class="admin-user-list">
                    <li>[<a href="/admin/edit-user/{{$user->id}}">Edit</a>]</li>
                    <li>User ID: {{$user->id}}</li>
                    <li><span class="@if($user->admin) text-success @else text-danger @endif">Admin</span></li>
                    <li><span class="@if($user->poster) text-success @else text-danger @endif">Approved Poster</span></li>
                </ul>
            </div>
        @endforeach
    </div>
</div>
@endsection
