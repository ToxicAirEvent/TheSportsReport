@extends('layouts.app')

@section('content')
	<?php
		//These variables will filter back to the app.blade layout and be interpereted as meta data for the page.
		$page_title = null;
		$page_meta_description = null;
	?>

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
				@foreach ($feed_data as $feed_item)
					<div id="{{$feed_item->highlight_id}}" class="single-post pvm phm">
						<h2><a href="{{$feed_item->permalink}}" class="title-link">{{$feed_item->highlight_title}}</a></h2>

						@include('post_components.poster_card',[
							'poster' => $feed_item->poster_info,
							'post_time' => $feed_item->human_time,
							'post_type' => $feed_item->service_origin,
							]
						)

						<div>
							@if($feed_item->service_origin === 'organic_video')
								<video class="video-post" controls>
								@if($feed_item->optimized_file === null)
									<source src="{{$feed_item->origin_url}}" type="video/mp4">
								@else
									<source src="{{$feed_item->optimized_file}}" type="video/mp4">
								@endif
								</video>
							@else
								{!! $feed_item->embed_data !!}
							@endif
						</div>
						<p>{!!$feed_item->highlight_description!!}</p>
						<p>
							<span class="share-btn">
								<span tabindex="0"
									class="btn-link pointer-cursor"
									role="button" data-toggle="popover"
									data-html="true"
									title="Share: <b>{{$feed_item->highlight_title}}</b>"
									data-content="<p>From: {{$feed_item->origin_url}}</p> <p>Here: {{$feed_item->share_link}}</p>">
									Share
								</span>
							</span>
						</p>
					</div>
				@endforeach
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
				@include('filters.postFilters')
			</div>
		</div>

		<div class="row mtl">
			<div class="col-xs-12 col-md-12"> 
				{{--Pagination Links Generator--}}
				{{ $feed_data->links() }}
			</div>
		</div>
	</div>

	<script>
	window.onload = function () {
	$('[data-toggle="popover"]').popover({
		container: '.single-post'
	});
    jQuery('.share-btn').css('opacity', '1');
};
</script>

@endsection