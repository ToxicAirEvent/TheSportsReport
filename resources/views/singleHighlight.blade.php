@extends('layouts.app')

@if($highlight_data->service_origin === 'organic_video')
	@section('open_graph_tags')
		<meta property="og:title" content="{{$highlight_data->highlight_title}}" />
		<meta property="og:description" content="{!! $highlight_data->highlight_description !!}" />

		@if($highlight_data->optimized_file === null)
			<meta property="og:video" content="/videos/{{$highlight_data->origin_url}}" />
		@else
			<meta property="og:video" content="/videos/{{$highlight_data->optimized_file}}" />
		@endif
	@endsection
@endif

@section('content')

	<?php
		//These variables will filter back to the app.blade layout and be interpereted as meta data for the page.
		$page_title = $highlight_data->highlight_title;
		$page_meta_description = strip_tags($highlight_data->highlight_description);
	?>

	<div class="container">
		<div class="row">

			@include('common.errors')

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h2>{{$highlight_data->highlight_title}}</h2>

				@include('post_components.poster_card',
					[
						'poster' => $highlight_data->poster_info,
						'post_time' => $highlight_data->human_time,
						'post_type' => $highlight_data->service_origin
					])

				<div>
					@if($highlight_data->service_origin === 'organic_video')
						<video class="video-post" controls>
							@if($highlight_data->optimized_file === null)
								<source src="{{$highlight_data->origin_url}}" type="video/mp4">
							@else
								<source src="{{$highlight_data->optimized_file}}" type="video/mp4">
							@endif
						</video>
					@else
						{!! $highlight_data->embed_data !!}
					@endif
				</div>
				<p>{!! $highlight_data->highlight_description !!}</p>
					@if($highlight_data->service_origin === 'organic_video')
						@include('post_components.organic_video', ['vid' => $highlight_data])
					@else
						<p class="text-muted">
							Found on <span class="text-capitalize">{{$highlight_data->service_origin}}</span>
							(<a href="{{$highlight_data->origin_url}}" target="_blank">{{$highlight_data->origin_url}}</a>) | Posted: 
							@if($highlight_data->created_at == null)
								Unknown
							@else
								{{$highlight_data->created_at}}
							@endif
						</p>
					@endif
				<hr>
			</div>

			@if(count($highlight_data->post_tags) != 0)
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<p>Tagged:
					@foreach($highlight_data->post_tags as $post_tag)
						<a href="/tagged/{{$post_tag['tag_url']}}" class="badge badge-info">{{$post_tag['tag_read']}}</a>
					@endforeach
				</p>
			</div>
			@endif
		</div>

		{{-- Only admins can delete or tag an highlight. Users who created the highlight can also tag or delete it. --}}
		{{-- An additional check of it being a foreign post is done just to prevent any weird behavior if two user IDs aligned between servers. --}}
		@if($highlight_data->is_admin || (!isset($highlight_data->foreign_post) && $highlight_data->authed_id == $highlight_data->poster_id))
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<form action="/new-tag-set" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					<input type="hidden" name="highlight_id" value="{{$highlight_data->highlight_id}}">
					Tags: <input type="text" name="tags" maxlength="100">
					<button type="submit" class="btn btn-success">Add Tags</button>
					<p class="text-warning">Maximum 100 characters. Tags are seperated with commas.</p>
				</form>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<form action="/delete-highlight" method="POST" class="form-horizontal" onsubmit="return confirm('Are you sure you want to delete this?');">
					{{ csrf_field() }}
					<input type="hidden" name="highlight_unique_id" value="{{$highlight_data->highlight_id}}">
					<button type="submit" class="btn btn-danger">Delete This Highlight</button>
					<p class="text-warning">There is no going back from this!</p>
				</form>
			</div>
		</div>
		@endif
	</div>
@endsection